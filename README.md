<meta charset="UTF-8">

# Python
---
Links uteis:

[Python](https://www.python.org/)

[Pypi](https://pypi.python.org/pypi)





## Hello World!!
Escrevendo na tela

`print("Hello World")`




## Executando Arquivo
Executando um arquivo python:

`python file.py`




## Comentários
Comentários de uma linha:

`#Comentário`

Comentários de múltiplas linhas:
```
"""
Comentário
"""
```




## Indentação
- Obrigatório para o funcionamento do programa
- Por padrão adota-se tabulações com tamanho de 4 espaços
- As instruções acabam quando é encontrado um "/n" ou ";", no entanto essa segunda não é muito recomendada por poluir muito o código

Exemplo:
```
if True:
    print("Hello");
```




## Variáveis
Possuem como principais características: 
- um tipo
- um nome
- um valor
- um tamanho

Valores inteiros: `my_int = 5`

Valores decimais: `my_dec = 5.5`

Valores de string: `my_str = "texto"`

Para descobrir o **tipo** de uma determinada variável basta usar a instrução `type()`, ex:

`type(my_var)`





## Nomenclatura
Variáveis:
```
var = 
var1 = 
my_var =   
```

Classes:
```
Class
MyClass
Classe
```

Funções/Métodos:
```
function(param)
my_function(my_param)
```

Constantes:
```
PI
VALOR_MAXIMO
```

Pacotes:
```
os
package
```




## Concatenação de dados

Para valores inteiros:

```
num_int = 5

print("Valor = ", num_int)
print("Valor = %i" %num_int)
print("Valor = " + str(num_int))
```

Para valores decimais:

```
num_dec = 7.3
num_dec2 = 3.3333333333

print("Valor = ", num_dec)
print("Valor = %f" %num_int)
print("Valor = %.2f" %num_dec2)
print("Valor = " + str(num_dec))
```

Para strings:
```
my_str = "text"

print("Valor = ", my_str)
print("Valor = %s" %my_str)
print("Valor = " + my_str)
```

Para multiplos valores:
```
my_str = "text1"
my_str2 = "text2"
print("Valores = %s - e - %s" %(my_str, my_str2))
```




## Entrada de Dados
Leitura do teclado através do terminal:

```
my_var = input("Digite algo: ")
print(my_var)
```



# Operações Matemáticas
Soma:

`print(10+10+(20+7))`

Subtração:

`print(10-10)`

Multiplicação:

`print(10*10)`

Divisão:

`print(10/10)`

Valor inteiro de uma fração:

`print(10//6)`













